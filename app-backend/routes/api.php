<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', 'UserController@register');
Route::post('login', 'UserController@login');
Route::get('profile','UserController@getAuthenticatedUser');
Route::put('editUser/{id}', 'UserController@update');
Route::get('comments', 'CommentController@index');
Route::get('comments/{id}', 'CommentController@show');
Route::post('comments', 'CommentController@store');
Route::put('comments/{id}', 'CommentController@update');
Route::delete('comments/{id}', 'CommentController@delete');
Route::get('getCommentsNumber', 'UserController@getCommentsNumber');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
