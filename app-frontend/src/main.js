import React from 'react';
import { Switch, Route } from 'react-router-dom';

import LandingPage from './components/landingpage';
import AboutMe from './components/aboutme';
import Contact from './components/contact';
import Project from './components/project';
import News from './components/news';
import Coordinator from './components/coordinator';
import Profile from './login/Profile';

const Main = () => (
  <Switch>
    <Route path="/landing" component={LandingPage} />
    <Route path="/aboutme" component={AboutMe} />
    <Route path="/contact" component={Contact} />
    <Route path="/project" component={Project} />
    <Route path="/news" component={News} />
    <Route path="/coordinator" component={Coordinator} />
  </Switch>
)

export default Main;
