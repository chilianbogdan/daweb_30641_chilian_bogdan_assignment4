// import React, { Component } from 'react';
// import './App.css';
// import {Layout, Header, Navigation, Drawer, Content, Cell} from 'react-mdl';
// import Main from './components/main';
// import { Link } from 'react-router-dom';
// //import { createI18n, I18nProvider, withI18n } from 'react-portfolio-master'
// import {FormattedMessage, FormattedHTMLMessage} from 'react-intl';
// //export let variableOne;
// import {}
// const setLanguage=(language)=>{
//     window.localStorage.setItem('language',language);
//     window.location.reload();
// }
// class App extends Component {
//
//   render() {
//
//     return (
//
//       <div className="demo-big-content">
//     <Layout>
//         <Header className="header-color" title={<Link style={{textDecoration: 'none', color: 'white'}} to="/"><FormattedMessage id="app.learn-react-link"
//                                                                                                                                 defaultMessage="My Profile"
//                                                                                                                                 description="Link on react page"/></Link>} scroll>
//             <Navigation>
//                 <Link to="/"><FormattedMessage id="app.home"
//                                                defaultMessage="Home"
//                                                description="Link on react page"/></Link>
//                 <Link to="/news"><FormattedMessage id="app.news"
//                                                    defaultMessage="News"
//                                                    description="Link on react page"/></Link>
//                 <Link to="/project"><FormattedMessage id="app.project"
//                                                       defaultMessage="About project"
//                                                       description="Link on react page"/></Link>
//                 <Link to="/aboutme"><FormattedMessage id="app.about"
//                                                       defaultMessage="About me"
//                                                       description="Link on react page"/></Link>
//                 <Link to="/coordinator"><FormattedMessage id="app.coordinator"
//                                                           defaultMessage="Coordinator"
//                                                           description="Link on react page"/></Link>
//                 <Link to="/contact"><FormattedMessage id="app.contact"
//                                                       defaultMessage="Contact"
//                                                       description="Link on react page"/></Link>
//                 <button className="button" id="save" onClick={() =>setLanguage('ro')}>RO
//                 </button>
//                 <button className="button" id="save" onClick={() =>setLanguage('en')}>EN
//                 </button>
//
//             </Navigation>
//         </Header>
//         <Drawer title={<Link style={{textDecoration: 'none', color: 'black'}} to="/"><FormattedMessage id="app.learn-react-link"
//                                                                                                        defaultMessage="My Profile"
//                                                                                                        description="Link on react page"/></Link>}>
//             <Navigation>
//                 <Link to="/"><FormattedMessage id="app.home"
//                                                defaultMessage="Home"
//                                                description="Link on react page"/></Link>
//                 <Link to="/news"><FormattedMessage id="app.news"
//                                                    defaultMessage="News"
//                                                    description="Link on react page"/></Link>
//                 <Link to="/project"><FormattedMessage id="app.project"
//                                                       defaultMessage="About project"
//                                                       description="Link on react page"/></Link>
//                 <Link to="/aboutme"><FormattedMessage id="app.about"
//                                                       defaultMessage="About me"
//                                                       description="Link on react page"/></Link>
//                 <Link to="/coordinator"><FormattedMessage id="app.coordinator"
//                                                           defaultMessage="Coordinator"
//                                                           description="Link on react page"/></Link>
//                 <Link to="/contact"><FormattedMessage id="app.contact"
//                                                       defaultMessage="Contact"
//                                                       description="Link on react page"/></Link>
//
//             </Navigation>
//         </Drawer>
//         <Content>
//             <div className="page-content" />
//             <Main/>
//         </Content>
//     </Layout>
//
// </div>
//
//     );
//   }
// }
//
// export default App;
import React, { Component } from 'react'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import Main from './main';
import Navbar from './login/Navbar'
// import Landing from './login/Landing'
import Login from './login/Login'
import Register from './login/Register'
import Profile from './login/Profile'
// import {Content} from "react-mdl";
import LandingPage from "./components/landingpage";
import AboutMe from "./components/aboutme";
import Contact from "./components/contact";
import Project from "./components/project";
import News from "./components/news";
import Coordinator from "./components/coordinator";

class App extends Component {
    render() {
        return (
            <div>
            <Router>
                <div className="App">
                    <Navbar />

                    <div className="container">
                        <Route exact path="/register" component={Register} />
                        <Route exact path="/login" component={Login} />
                        <Route exact path="/profile" component={Profile} />
                        <Route path="/landing" component={LandingPage} />
                        <Route path="/aboutme" component={AboutMe} />
                        <Route path="/contact" component={Contact} />
                        <Route path="/project" component={Project} />
                        <Route path="/news" component={News} />
                        <Route path="/coordinator" component={Coordinator} />
                    </div>
                </div>
            </Router>

                {/*<Content>*/}
                {/*    <div className="page-content" />*/}
                {/*    <Main/>*/}
                {/*</Content>*/}

            </div>
        )
    }
}

export default App