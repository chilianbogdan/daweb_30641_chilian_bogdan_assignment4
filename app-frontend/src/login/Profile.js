import React, { Component } from 'react'
import { getProfile } from './UserFunctions'
import '../App.css';
import {Layout, Header, Navigation, Drawer, Content, Cell, Grid} from 'react-mdl';
import Main from '../main';
import { Link } from 'react-router-dom';
//import { createI18n, I18nProvider, withI18n } from 'react-portfolio-master'
import {FormattedMessage, FormattedHTMLMessage} from 'react-intl';
import App from "../App";
//export let variableOne;

const setLanguage=(language)=>{
    window.localStorage.setItem('language',language);
    window.location.reload();
}
class Profile extends Component {

  render() {

    return (

        <div className="All">

                <Header className="header-color" title={<Link style={{textDecoration: 'none', color: 'white'}} to="/profile"><FormattedMessage id="app.learn-react-link"
                                                                                                                                        defaultMessage="My Profile"
                                                                                                                                        description="Link on react page"/></Link>} scroll>
                    <Navigation>
                        <Link to="/landing"><FormattedMessage id="app.home"
                                                       defaultMessage="Home"
                                                       description="Link on react page"/></Link>
                        <Link to="/news"><FormattedMessage id="app.news"
                                                           defaultMessage="News"
                                                           description="Link on react page"/></Link>
                        <Link to="/project"><FormattedMessage id="app.project"
                                                              defaultMessage="About project"
                                                              description="Link on react page"/></Link>
                        <Link to="/aboutme"><FormattedMessage id="app.about"
                                                              defaultMessage="About me"
                                                              description="Link on react page"/></Link>
                        <Link to="/coordinator"><FormattedMessage id="app.coordinator"
                                                                  defaultMessage="Coordinator"
                                                                  description="Link on react page"/></Link>
                        <Link to="/contact"><FormattedMessage id="app.contact"
                                                              defaultMessage="Contact"
                                                              description="Link on react page"/></Link>
                        <button className="button" id="save" onClick={() =>setLanguage('ro')}>RO
                        </button>
                        <button className="button" id="save" onClick={() =>setLanguage('en')}>EN
                        </button>

                    </Navigation>
                </Header>
                {/*<Drawer title={<Link style={{textDecoration: 'none', color: 'black'}} to="/"><FormattedMessage id="app.learn-react-link"*/}
                {/*                                                                                               defaultMessage="My Profile"*/}
                {/*                                                                                               description="Link on react page"/></Link>}>*/}
                {/*    <Navigation>*/}
                {/*        <Link to="/landing"><FormattedMessage id="app.home"*/}
                {/*                                       defaultMessage="Home"*/}
                {/*                                       description="Link on react page"/></Link>*/}
                {/*        <Link to="/news"><FormattedMessage id="app.news"*/}
                {/*                                           defaultMessage="News"*/}
                {/*                                           description="Link on react page"/></Link>*/}
                {/*        <Link to="/project"><FormattedMessage id="app.project"*/}
                {/*                                              defaultMessage="About project"*/}
                {/*                                              description="Link on react page"/></Link>*/}
                {/*        <Link to="/aboutme"><FormattedMessage id="app.about"*/}
                {/*                                              defaultMessage="About me"*/}
                {/*                                              description="Link on react page"/></Link>*/}
                {/*        <Link to="/coordinator"><FormattedMessage id="app.coordinator"*/}
                {/*                                                  defaultMessage="Coordinator"*/}
                {/*                                                  description="Link on react page"/></Link>*/}
                {/*        <Link to="/contact"><FormattedMessage id="app.contact"*/}
                {/*                                              defaultMessage="Contact"*/}
                {/*                                              description="Link on react page"/></Link>*/}

                {/*    </Navigation>*/}
                {/*</Drawer>*/}
                <Content>
                    <div className="page-content" />
                    <Main/>
                </Content>

            <div>
                <Grid className="">
                    <Cell col={12}>



                        <div className="banner-text">
                            <h1><FormattedMessage id="landingpage.learn-react-link"
                                                  defaultMessage="Full Stack Web Developer"
                                                  description="Link on react page"/></h1>

                            <hr/>



                            {/*<div className="social-links">*/}

                            {/*  /!* LinkedIn *!/*/}
                            {/*  <a href="http://google.com" rel="noopener noreferrer" target="_blank">*/}
                            {/*    <i className="fa fa-linkedin-square" aria-hidden="true" />*/}
                            {/*  </a>*/}

                            {/*  /!* Github *!/*/}
                            {/*  <a href="http://google.com" rel="noopener noreferrer" target="_blank">*/}
                            {/*    <i className="fa fa-github-square" aria-hidden="true" />*/}
                            {/*  </a>*/}

                            {/*  /!* Freecodecamp *!/*/}
                            {/*  <a href="http://google.com" rel="noopener noreferrer" target="_blank">*/}
                            {/*    <i className="fa fa-free-code-camp" aria-hidden="true" />*/}
                            {/*  </a>*/}

                            {/*  /!* Youtube *!/*/}
                            {/*  <a href="http://google.com" rel="noopener noreferrer" target="_blank">*/}
                            {/*    <i className="fa fa-youtube-square" aria-hidden="true" />*/}
                            {/*  </a>*/}

                            {/*</div>*/}
                        </div>

                    </Cell>
                </Grid>
            </div>
        </div>
    );
  }
}
export default Profile