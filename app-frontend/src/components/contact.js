import React, { Component } from 'react';
import { Grid, Cell, List, ListItem, ListItemContent } from 'react-mdl';
import {FormattedMessage} from "react-intl";


class Contact extends Component {
  constructor(props) {
    super(props);
    this.state = {
      latitude: null,
      longitude: null,
      userAddress: null,
    };
    this.getLocation = this.getLocation.bind(this);
    this.getCoordinates = this.getCoordinates.bind(this);
  }

  getLocation() {
    if (navigator.geolocation) {
      navigator.geolocation.getCurrentPosition(this.getCoordinates);
    } else {
      alert("Location is not supported by this browser. Use another one!");
    }
  }

  getCoordinates(position) {
    this.setState({
      latitude: position.coords.latitude,
      longitude: position.coords.longitude,
    });
  }
  render() {
    this.getLocation();
    return(
      <div className="contact-body">
        <Grid className="contact-grid">
          <Cell col={6}>
            <h2>Bogdan Chilian</h2>
            <img
              src="https://www.facebook.com/chilian.bogdan?viewas=100000686899395&privacy_source=timeline_gear_menu&entry_point=action_bar#_"
              alt="avatar"
              style={{height: '250px'}}
               />
             <p style={{ width: '75%', margin: 'auto', paddingTop: '1em'}}>23 years old</p>
            <div className="emailForm">
              <form action="http://localhost:5000/result" method="get">
                <FormattedMessage id="contact.email"
                                  defaultMessage="Send an email to:"
                                  description="Link on react page"/>
                {" "}
                <input className="textfieldEmail" type="text" name="place" />
                <input className="submitEmail" type="submit" value="Submit" />
              </form>
            </div>

            <h3 className="contact-pagina">
              <p className="contact-scris">


                  Address:


                <div>
              Current Latitude: {this.state.latitude}
              <br />
              Current Longitude: {this.state.longitude}
            </div>
                <iframe
                    className="Map"
                    src={
                      "https://www.google.com/maps/embed?pb=!1m24!1m12!1m3!1d5489.81566713736!2d24.567311977342346!3d46.52972561651182!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!4m9!3e6!4m3!3m2!1d" +
                      this.state.latitude +
                      "!2d" +
                      this.state.longitude +
                      "!4m3!3m2!1d" +
                      46.770836 + //// latitudine adresa contact
                      "!2d" +
                      23.555308 + ///longitudine adresa contact
                      "!5e0!3m2!1sen!2sro!4v1590394200328!5m2!1sen!2sro"
                    }
                    width="600"
                    height="400"
                ></iframe>
                Address: Str.  Stefan Mora   nr.8   sc.A1   City: Cluj-Napoca   Jud. Cluj
              </p>
            </h3>




          </Cell>
          <Cell col={6}>
            <h2><FormattedMessage id="contact.contactme"
                                  defaultMessage="Contact Me:"
                                  description="Link on react page"/></h2>
            <hr/>

            <div className="contact-list">
              <List>
                <ListItem>
                  <ListItemContent style={{fontSize: '30px', fontFamily: 'Anton'}}>
                    <i className="fa fa-phone-square" aria-hidden="true"/>
                    0752 684 224
                  </ListItemContent>
                </ListItem>

                {/*<ListItem>*/}
                {/*  <ListItemContent style={{fontSize: '30px', fontFamily: 'Anton'}}>*/}
                {/*    <i className="fa fa-fax" aria-hidden="true"/>*/}
                {/*    (123) 456-7890*/}
                {/*  </ListItemContent>*/}
                {/*</ListItem>*/}

                <ListItem>
                  <ListItemContent style={{fontSize: '30px', fontFamily: 'Anton'}}>
                    <i className="fa fa-envelope" aria-hidden="true"/>
                    chilian.bogdan@gmail.com
                  </ListItemContent>
                </ListItem>

                <ListItem>
                  <ListItemContent style={{fontSize: '20px', fontFamily: 'Anton'}}>
                    <i className="fa fa-facebook" aria-hidden="true"/>
                    https://www.facebook.com/chilian.bogdan
                  </ListItemContent>
                </ListItem>

                <ListItem>
                  <ListItemContent style={{fontSize: '15px', fontFamily: 'Anton'}}>
                    <i className="fa fa-linkedin" aria-hidden="true"/>
                    https://ro.linkedin.com/in/bogdan-chilian-64b52916b
                  </ListItemContent>
                </ListItem>

                <ListItem>
                  <ListItemContent style={{fontSize: '20px', fontFamily: 'Anton'}}>
                    <i className="fa fa-instagram" aria-hidden="true"/>
                    https://www.instagram.com/chilianbogdan
                  </ListItemContent>
                </ListItem>

                <ListItem>
                  <ListItemContent style={{fontSize: '20px', fontFamily: 'Anton'}}>
                    <i className="fa fa-twitter" aria-hidden="true"/>
                    https://twitter.com/ChilianBogdan
                  </ListItemContent>
                </ListItem>

              </List>
            </div>
          </Cell>
        </Grid>



      </div>
    )
  }
}

export default Contact;
