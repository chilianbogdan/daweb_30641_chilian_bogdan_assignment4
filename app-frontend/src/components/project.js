import React, { Component } from 'react';
import { Tabs, Tab, Grid, Cell, Card, CardTitle, CardText, CardActions, Button, CardMenu, IconButton } from 'react-mdl';
import { FormattedMessage } from 'react-intl';
import Chart from './Chart';
import List from './List'
import {getCommentsNumber, getList} from './ListFunctions'
class Project extends Component {
  constructor(props) {
    super(props);
    this.state = {
        chartData:{
            labels: [],
            datasets: [{label: "", data: [], backgroundColor: []}]
        },
        nr: [{num: "", created_at: ""}],
        activeTab: 0,
        term: '',
        items: []
    };

  }
    componentWillMount(){
       this.getAll()
    }

    getChartData(){

        const dis=[];

        console.log(this.state.nr);
        console.log(this.state.nr[0].num);
        // delete this.state.chartDAta
        // this.state.chartData=this.state.ch;
        // dis[0]=<Chart chartData={this.state.ch} location="Top " location2={this.state.nr} legendPosition="bottom"/>;
        for (let i = 0; i < this.resultsCount(); i++) {


            this.state.chartData.labels[i]=this.state.nr[i].created_at;
            this.state.chartData.datasets[0].label="Date";
            this.state.chartData.datasets[0].data[i]=this.state.nr[i].num;
            this.state.chartData.datasets[0].backgroundColor[i]='rgba(54, 162, 235, 0.6)';
             this.state.chartData.datasets[0].data[i+1]=0;
        }
        dis[0]=<Chart chartData={this.state.chartData} location="Top " location2={this.state.nr} legendPosition="bottom"/>;
        return dis;

    }
    getAll = () => {
        getCommentsNumber().then(data => {
            this.setState(
                {
                    nr: [...data]
                },
                () => {
                    console.log(this.state.nr)
                }
            )
        })
        this.getChartData()
    }
    resultsCount() {
        let nResults;
        if (this.state.nr != null) {
            nResults = this.state.nr.length;
        }
        else {
            nResults = 0;
        }
        return nResults;
    }



  toggleCategories() {

    if(this.state.activeTab === 0){
      return(
          <div><h1><p>
            <FormattedMessage
                id="project.t"
                defaultMessage="It's a beautiful day outside."
                description="Link on react page"
            />
          </p></h1></div>


      )
    } else if(this.state.activeTab === 1) {
      return (
        <div><h1><p>
          <FormattedMessage
              id="project.t"
              defaultMessage="It's a beautiful day outside."
              description="Link on react page"
          />
        </p></h1></div>
      )
    } else if(this.state.activeTab === 2) {
      return (
        <div><h1><p>
          <FormattedMessage
              id="project.t"
              defaultMessage="It's a beautiful day outside."
              description="Link on react page"
          />
        </p></h1></div>
      )
    } else if(this.state.activeTab === 3) {
      return (
        <div><h1><p>
          <FormattedMessage
              id="project.t"
              defaultMessage="It's a beautiful day outside."
              description="Link on react page"
          />
        </p></h1></div>
      )
    }

  }




    render() {
    return(
      <div>
        <Tabs activeTab={this.state.activeTab} onChange={(tabId) => this.setState({ activeTab: tabId })} ripple>
          <Tab><FormattedMessage id="project.summary"
                                 defaultMessage="Summary"
                                 description="Link on react page"/></Tab>
          <Tab><FormattedMessage id="project.content"
                                 defaultMessage="Content"
                                 description="Link on react page"/></Tab>
          <Tab><FormattedMessage id="project.writing"
                                 defaultMessage="Writing"
                                 description="Link on react page"/></Tab>
          <Tab><FormattedMessage id="project.bibliography"
                                 defaultMessage="Bibliography"
                                 description="Link on react page"/></Tab>
        </Tabs>


          <Grid>
            <Cell col={12}>
              <div className="content">{this.toggleCategories()}</div>
            </Cell>
          </Grid>


                      <List />
          {/*<button className="PostComment" onClick={() => this.getAll()}>Generate Chart</button>*/}
          {this.getChartData()}

      </div>
    )
  }
}

export default Project;
