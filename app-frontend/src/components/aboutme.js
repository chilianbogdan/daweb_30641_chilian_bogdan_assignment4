import React, { Component } from 'react';
import {Cell} from "react-mdl";
import {FormattedMessage} from "react-intl";
import {addItem, updateUser} from "./ListFunctions";


class About extends Component {
    constructor() {
        super()
        this.state = {
            id: '',
            comment: '',
            artcomment: '',
            artbody: '',
            editDisabled: false,
            items: []
        }

        this.onChange = this.onChange.bind(this)
        this.onChange2 = this.onChange2.bind(this)
        this.onChange3 = this.onChange3.bind(this)
        this.onChange4 = this.onChange4.bind(this)
    }

    componentDidMount() {

    }

    onChange = e => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }
    onChange2 = e => {
        this.setState({
            [e.target.password]: e.target.value
        })
    }
    onChange3 = e => {
        this.setState({
            [e.target.domeniu]: e.target.value
        })
    }
    onChange4 = e => {
        this.setState({
            [e.target.email]: e.target.value
        })
    }


    onUpdate = e => {
        e.preventDefault()
        updateUser(this.state.name,this.state.password,this.state.domeniu, 1).then(() => {

        })
        this.setState({
            editDisabled: ''
        })
    }
  render() {
    return(
      <div> <h2 style={{paddingTop: '2em'}}>Bogdan Chilian</h2>
          <h4 style={{color: 'grey'}}>Student</h4>
          <hr style={{borderTop: '3px solid #833fb2', width: '50%'}}/>
          <p><FormattedMessage id="about.faculty"
                               defaultMessage="Faculty of Automation and Computer Science, Cluj-Napoca (Romania)"
                               description="Link on react page"/></p>
          <hr style={{borderTop: '3px solid #833fb2', width: '50%'}}/>
          <h5><FormattedMessage id="about.address"
                                defaultMessage="Address"
                                description="Link on react page"/></h5>
          <p><FormattedMessage id="about.add"
                               defaultMessage="Satu Mare, County: Satu Mare, Street: Bobocului, UK.21, Ap.3"
                               description="Link on react page"/></p>
          <h5><FormattedMessage id="about.phone"
                                defaultMessage="Phone"
                                description="Link on react page"/></h5>
          <p>0752684224</p>
          <h5>Email</h5>
          <p>chilian.bogdan@gmail.com</p>
          {/*<h5>Web</h5>*/}
          {/*<p>mywebsite.com</p>*/}
          <hr style={{borderTop: '3px solid #833fb2', width: '50%'}}/>
          <div className="form-group">
              <label htmlFor="name">name</label>
              <div className="row">
                  <div className="col-md-12">
                      <input
                          type="text"
                          className="form-control"
                          id="name"
                          name="name"
                          value={this.state.name}
                          onChange={this.onChange.bind(this)}
                      />
                  </div>
              </div>
          </div>
          <div className="form-group">
              <label htmlFor="email">email</label>
              <div className="row">
                  <div className="col-md-12">
                      <input
                          type="text"
                          className="form-control"
                          id="email"
                          name="email"
                          value={this.state.email}
                          onChange={this.onChange4.bind(this)}
                      />
                  </div>
              </div>
          </div>
          <div className="form-group">
              <label htmlFor="password">password</label>
              <div className="row">
                  <div className="col-md-12">
                      <input
                          type="text"
                          className="form-control"
                          id="password"
                          name="password"
                          value={this.state.password}
                          onChange={this.onChange2.bind(this)}
                      />
                  </div>
              </div>
          </div>
          <div className="form-group">
              <label htmlFor="domeniu">domain</label>
              <div className="row">
                  <div className="col-md-12">
                      <input
                          type="text"
                          className="form-control"
                          id="domeniu"
                          name="domeniu"
                          value={this.state.domeniu}
                          onChange={this.onChange3.bind(this)}
                      />
                  </div>
              </div>
          </div>
          <button
              type="submit"
              onClick={this.onUpdate.bind(this)}
              className="btn btn-success btn-block"
          >
              Update
          </button>


      </div>
    )
  }
}

export default About;
